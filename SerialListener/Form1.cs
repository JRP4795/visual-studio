﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using System.Timers;
using System.IO;

namespace SerialListener
{
    public partial class MainForm : Form
    {

        public string data { get; set; }

        public MainForm()
        {
            InitializeComponent();
            configrations();
        }

        public void configrations()
        {
            portConfig.Items.AddRange(SerialPort.GetPortNames());
            baudrateConfig.DataSource = new[] { "115200", "19200", "230400", "57600", "38400", "9600", "4800" };
            parityConfig.DataSource = new[] { "None", "Odd", "Even", "Mark", "Space" };
            databitsConfig.DataSource = new[] { "5", "6", "7", "8" };
            stopbitsConfig.DataSource = new[] { "1", "2", "1.5" };
            flowcontrolConfig.DataSource = new[] { "None", "RTS", "RTS/X", "Xon/Xoff" };
            baudrateConfig.SelectedIndex = 5;
            parityConfig.SelectedIndex = 0;
            databitsConfig.SelectedIndex = 3;
            stopbitsConfig.SelectedIndex = 0;
            flowcontrolConfig.SelectedIndex = 0;
            openFileDialog1.Filter = "All files|*.*";

            mySerial.DataReceived += rx_data_event;
            backgroundWorker1.DoWork += new DoWorkEventHandler(update_rxtextarea_event);

        }

        /*connect and disconnect*/
        private void connect_Click(object sender, EventArgs e)
        {
            /*Connect*/
            if (!mySerial.IsOpen)
            {
                if (Serial_port_config())
                {
                    try{
                        mySerial.Open();
                    }
                    catch
                    {
                        alert("Can't open " + mySerial.PortName + " port, it might be used in another program");
                        return;
                    }
                    UserControl_state(true);
                }
            }

            /*Disconnect*/
            else if (mySerial.IsOpen)
            {
                try
                {
                    mySerial.Close();
                    mySerial.DiscardInBuffer();
                    mySerial.DiscardOutBuffer();
                }
                catch {/*ignore*/}

                UserControl_state(false);
            }
        }

        /* RX -----*/

        /* read data from serial */
        private void rx_data_event(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            if (mySerial.IsOpen)
            {
                try
                {
                    int dataLength = mySerial.BytesToRead;
                    byte[] dataRecevied = new byte[dataLength];
                    int nbytes = mySerial.Read(dataRecevied, 0, dataLength);
                    if (nbytes == 0) return;

                    this.BeginInvoke((Action)(() =>
                    {
                        data = System.Text.Encoding.Default.GetString(dataRecevied);

                        if (!backgroundWorker1.IsBusy)
                        {
                            if (display_hex_radiobutton.Checked)
                                data = BitConverter.ToString(dataRecevied);

                            backgroundWorker1.RunWorkerAsync();
                        }
                    }));
                }
                catch { alert("Can't read form  " + mySerial.PortName + " port it might be opennd in another program"); }
            }
        }

        /* Append text to rx_textarea and log data*/
        private void update_rxtextarea_event(object sender, DoWorkEventArgs e)
        {
            this.BeginInvoke((Action)(() =>
            {
                DateTime localDate = DateTime.Now;

               if (rx_textarea.Lines.Count() > 5000) { rx_textarea.ResetText(); }

                rx_textarea.AppendText("[RX]> " + localDate.ToString("MM/dd/yyyy HH:mm:ss")+ ": " + data);

                if (datalogger_checkbox.Checked)
                {
                    try
                    {
                        StreamWriter sw = new StreamWriter(data_path.Text, true);
                        sw.Write(localDate.ToString("MM/dd/yyyy") +","+ localDate.ToString("HH:mm:ss")+","+data.Replace("\\n", Environment.NewLine));
                        sw.Close();
                    }
                    catch { alert("Can't write to " + data_path.Text + " file it might be not exist or it is opennd in another program"); return; }
                }
            }));
        }

        /* Enable data logger and log file selection */
        private void DHTlogger_checkbox_CheckedChanged(object sender, EventArgs e)
        {
            if (datalogger_checkbox.Checked)
            {
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    data_path.Text = openFileDialog1.FileName;
                }
                else
                {
                    datalogger_checkbox.Checked = false;
                }
            }
            else
            {
                datalogger_checkbox.Text = "Enable data logger";
                data_path.Text = "";
            }
        }

        /* clear rx textarea */
        //private void clear_rx_textarea_Click(object sender, EventArgs e)
        //{
        //    rx_textarea.Clear();
        //}

        /*Application-----*/
        /*serial port config*/
        private bool Serial_port_config()
        {
            try { mySerial.PortName = portConfig.Text; }
            catch { alert("There are no available ports"); return false; }
            mySerial.BaudRate = (Int32.Parse(baudrateConfig.Text));
            mySerial.StopBits = (StopBits)Enum.Parse(typeof(StopBits), (stopbitsConfig.SelectedIndex + 1).ToString(), true);
            mySerial.Parity = (Parity)Enum.Parse(typeof(Parity), parityConfig.SelectedIndex.ToString(), true);
            mySerial.DataBits = (Int32.Parse(databitsConfig.Text));
            mySerial.Handshake = (Handshake)Enum.Parse(typeof(Handshake), flowcontrolConfig.SelectedIndex.ToString(), true);

            return true;
        }

        private void UserControl_state(bool value)
        {
            serial_options_group.Enabled = !value;
            datalogger_options_panel.Enabled = !value;
            data_path.Enabled = !value;

            if (value)
            {
                connect.Text = "Disconnect";
                toolStripStatusLabel1.Text = "Connected port: " + mySerial.PortName + " @ " + mySerial.BaudRate + " bps";
            }
            else
            {
                connect.Text = "Connect";
                toolStripStatusLabel1.Text = "No Connection";
            }
        }

        /*alert function*/
        private void alert(string text)
        {
            alert_messege.Icon = Icon;
            alert_messege.Visible = true;
            alert_messege.ShowBalloonTip(5000, "Serial Lab", text, ToolTipIcon.Error);
        }

        private void Rx_textarea_MouseClick(object sender, MouseEventArgs e)
        {
            rx_textarea.Clear();
        }
    }
}
